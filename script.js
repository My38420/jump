// Create Scene
const root = document.getElementById('root');
root.style.display = 'flex';
root.style.justifyContent = 'center';
root.style.alignItems = 'center';
const screen = document.createElement('div');
screen.style.position = "relative";
screen.style.width = "700px";
screen.style.height = "400px";
screen.style.border = "solid 1px black";
screen.style.background = "linear-gradient(to bottom, #94c5f8 1%,#a6e6ff 70%,#b1b5ea 100%)";

const floor = document.createElement('div');
floor.style.position = "absolute";
floor.style.bottom = 0;
floor.style.width = "700px";
floor.style.height = "10px";
floor.style.border = "solid 1px green";
floor.style.backgroundColor = "green";
screen.appendChild(floor);

const box = document.createElement('div');
box.style.position = "absolute";
box.style.bottom = "12px";
box.style.right = "25px";
box.style.width = "80px";
box.style.height = "120px";
box.style.backgroundColor = "#eae060";
box.style.border = "solid 2px #bbb44f";
box.style.borderRadius = "5px";
screen.appendChild(box);

const perso = document.createElement('div');
perso.style.position = "absolute";
perso.style.bottom = "12px";
perso.style.left = "25px";
perso.style.width = "30px";
perso.style.height = "30px";
perso.style.backgroundColor = "red";
perso.style.border = "solid 2px black";
perso.style.borderRadius = "20px";
screen.appendChild(perso);

root.appendChild(screen);


// Make the character move and jump!
var keys = {
    right: false,
    left: false,
    up:false
};

function moveDir(nomTouche) {
    // flèche droite du clavier : déplacement à doite
    // flêche gauche relachée : déplacement à gauche
    console.log("moveDir");
    console.log(event);
    let posLeft = parseInt(perso.style.left);
    let posBottom = parseInt(perso.style.bottom);
    if (nomTouche === "ArrowRight") {
        if (posLeft < 400){
            posLeft = posLeft + 10;
            console.log("right");
            console.log(posLeft);
        }
       
    }
    if (nomTouche === "ArrowLeft") {
        if (posLeft > 10){
            posLeft = posLeft - 10;
            console.log("left");
            console.log(posLeft);
        }
        
    }
    if (nomTouche === "ArrowUp") {
        
        if (posBottom < 370){
            posBottom = posBottom + 30;
            console.log("jump");
            console.log(posBottom);
        }
        
    }
    perso.style.left = posLeft.toString();
    perso.style.bottom = posBottom.toString();
    console.log(perso.style.left)
}
function jump(dir) {
    // flèche droite du clavier + flêche up: déplacement en haut à doite
    // flêche gauche relachée + flêche up: déplacement en haut à gauche
    console.log("jump");
    console.log(dir);
    let posLeft = parseInt(perso.style.left);
    let posBottom = parseInt(perso.style.bottom);
    if (dir="jumpLeft" && posBottom < 370){
        if (posLeft > 10){
            posLeft = posLeft - 10;
            console.log("left");
            console.log(posLeft);
        }
        posBottom = posBottom + 30;
        console.log("jump");
        console.log(posBottom);
    }
    else if (dir="jumpRight" && posBottom < 370){
        if (posLeft < 400){
            posLeft = posLeft + 10;
            console.log("right");
            console.log(posLeft);
        }
        posBottom = posBottom + 30;
        console.log("jump");
        console.log(posBottom);
    } 
    
    perso.style.left = posLeft.toString();
    perso.style.bottom = posBottom.toString();
    console.log(perso.style.left)
}
const map = { 37: false, 38: false, 39: false };

document.addEventListener('keyup', (event) => {
    const nomTouche = event.key;
    if (event.keyCode in map) {
        map[event.keyCode] = true;
        if (map[37] && map[38]) {
            // Write your own code here, what  you want to do
            map[37] = false;
            map[38] = false;
            let dir = "jumpLeft"
            jump(dir)
            
        }
        else if (map[39] && map[38]) {
            // Write your own code here, what  you want to do
            map[39] = false;
            map[38] = false;
            let dir = "jumpRight"
            jump(dir)
            
        }
    }
    else {
        // if u press any other key apart from that "map" will reset.
        map[37] = false;
        map[38] = false;
        map[39] = false;
    }
    // Dès que l'utilisateur relâche la touche arrowRight, la touche n'est plus active.
    // Aussi event.ctrlKey est false.
    if (nomTouche === 'ArrowRight') {
        moveDir(nomTouche)
    }
    if (nomTouche === 'ArrowLeft') {
        moveDir(nomTouche)
      }
    if (nomTouche === 'ArrowUp') {
        // alert('La touche ArrowUp a été relâchée');
        moveDir(nomTouche)
      }
  }, false);
// window.addEventListener('keydown', moveDir);